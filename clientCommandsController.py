# This file is where the client commands are implemented. All client commands should have a method in this file that
# carries out the action/command.

# global variable
rg_groupname = ''

# This is the controller method for the 'help' command. It takes in a "name" that tells us if the user is logged in yet.
def help_controller(name):
    # user is not logged in
    if name == None:
        print("Available commands: \n\n"
              "    login ID    -  This logs a user in, where ID is the user ID.\n"
              "    help        -  This prints the list of commands/subcommands available.\n")
    else:
        # user is logged in; print commands.
        print("Available commands: \n\n"
              "    ag [N]        -  This lists the names of all existing discussion groups.\n"
              "                     N is an optional argument that lists N groups at a time.\n\n"
              "        subcommands: \n\n"
              "            s 1 [2] ... [N] -  This allows the user to subscribe to group 1, 2, ... N.\n"
              "                               Note that there needs to be at least one argument, and \n"
              "                               the number cannot exceed N.\n"
              "            u 1 [2] ... [N] -  This allows the user to unsubscribe to group 1, 2, ... N.\n"
              "                               Note that there needs to be at least one argument, and \n"
              "                               the number cannot exceed N.\n"
              "            n               -  This lists the next N discussion groups. If all groups are\n"
              "                               displayed, the command ag is exited.\n"
              "            q               -  This exits from the ag command.\n\n"
              "    sg [N]        -  This lists all of the subscribed groups the user is in.\n"
              "                     Note that N is an optional argument that lists N groups at a time.\n\n"
              "        subcommands: \n\n"
              "            u 1 [2] ... [N] -  This allows the user to unsubscribe to group 1, 2, ... N.\n"
              "                               Note that there needs to be at least one argument, and \n"
              "                               the number cannot exceed N.\n"
              "            n               -  This lists the next N discussion groups. If all groups are \n"
              "                               displayed, the command sg is exited.\n"
              "            q               -  This exits from the sg command.\n\n"
              "    rg gname [N]  -  This lists all of the posts in the group gname.\n"
              "                     Note that N is an optional argument that lists N posts at a time.\n\n"
              "        subcommands: \n\n"
              "            [id]     -  This is a number between 1 and N that shows the content of the post \n"
              "                        whose id is between 1 and N.\n\n"
              "                subcommands: \n\n"
              "                    n  -  This displays at most N more lines of the post's content.\n"
              "                    q  -  This quits displaying the post content.\n\n"
              "            r 1[-3]  -  This marks a post (1) or a range of posts (1-3) as read.\n"
              "                        Note that the post numbers and ranges must be between 1 and N.\n"
              "            n        -  This lists the next N posts. If all posts are displayed, the command\n"
              "                        rg is exited.\n"
              "            p        -  This allows the user to post to the group. The first line is the\n"
              "                        subject of the post, and the next line(s) denote the content.\n"
              "                        A post is considered completed when there is a dot by itself on\n"
              "                        a line (\"\\n.\\n\")\n"
              "            q        -  This exits from the rg command.\n\n"
              "    logout        -  This logs out the user and terminates the client program.\n")


# This is the controller for the 'logout' command. The client sends logout to the server, and waits for 'bye' back.
# It takes in the client's name and the client socket, and returns true on success, false otherwise
def logout_controller(name, clientSocket):
    # send the logout message
    message = 'logout'
    clientSocket.send(message.encode())
    message = clientSocket.recv(1024)
    message = message.decode()

    if(message == 'bye'):
      print ('logging out and exiting...')
      clientSocket.close()


# This is the controller for when a client wants to view all groups in the system. It sends the command with the number
# of posts it wants to view at a time as an optional argument, and prints the data the server returns until the server
# sends the end deliminator.
def ag_controller(clientSocket, command):
    clientSocket.send(command.encode())

    data = clientSocket.recv(1024)
    strdata = data.decode()
    print(strdata)

# This is the controller for when a client wants to subscribe to a new group. It sends the command with the group numbers
# the user wants to subscribe to.
def s_controller(clientSocket, command):
    clientSocket.send(command.encode())

    data = clientSocket.recv(1024)
    strdata = data.decode()
    print(strdata)

# This is the controller for when a client wants to view the next N groups.
def n_controller(clientSocket, command):
    clientSocket.send(command.encode())

    data = clientSocket.recv(1024)
    strdata = data.decode()
    if strdata == "exit":
      return strdata
    else:
      print(strdata)

# This is the controller for when a client wants to unsubscribe from a group. It sends the command with the group numbers
# the user wants to unsubscribe from.
def u_controller(clientSocket, command):
    clientSocket.send(command.encode())

    data = clientSocket.recv(1024)
    strdata = data.decode()
    print(strdata)

# This is the controller for when a client wants to view the groups they are subscribed in. It sends the command with
# the number of posts it wants to view at a time as an optional argument, and prints the data the server returns.
def sg_controller(clientSocket, command):
    clientSocket.send(command.encode())

    data = clientSocket.recv(1024)
    strdata = data.decode()
    print(strdata)

# This is the controller for when a client wants to read the posts from groups they are subscribed to. It sends the command with
# the group name and the number of posts it wants to view at a time as an optional argument, and prints the data the server returns.
def rg_controller(clientSocket, command):
    # set the group name, and tape the command back together
    global rg_groupname
    data = command.split(' ')
    rg_groupname = data[1]

    n_command = ''
    for d in data:
        n_command += d + ' '
    # remove trailing whitespace
    n_command = n_command[:-1]

    clientSocket.send(n_command.encode())

    data = clientSocket.recv(1024)
    strdata = data.decode()
    print(strdata)

    if "Error" in strdata:
        return "exit"

# This is the controller for when a client wants to mark a post as read
def r_controller(clientSocket, command):
    clientSocket.send(command.encode())

    data = clientSocket.recv(1024)
    strdata = data.decode()
    print(strdata)

# This is the controller for when a client wants to make a new post. For simplicity purposes, the write-up of the post
# will be entirely in this controller.
# sent to server: 'p \r\n\r\n[groupname]\r\n\r\n[subject]\r\n\r\n[content]\n.\n'
def p_controller(clientSocket, command):
    data = command + ' \r\n\r\n' + rg_groupname + '\r\n\r\n'
    subject = input('Please enter the subject of the post: ')
    data += subject + '\r\n\r\n'
    print('Now, write the content of the post. To finish, print a dot (''.'') on a line by itself.')
    content = ''
    while content is not '.':
        content = input('>')
        data += content + '\n'

    print(data)
    clientSocket.send(data.encode())

    data = clientSocket.recv(1024)
    strdata = data.decode()
    if strdata == 'p OK':
        print('New post created.')
    else:
        print(strdata)

# This is the controller for when a client wants to quit a subcommand. It sends the command to the server so the server
# knows to exit the main command.
def q_controller(clientSocket, command, last_main_command):
    global rg_groupname
    if last_main_command != '1':
        rg_groupname = ''
    clientSocket.send(command.encode())

def id_controller(clientSocket, command):
    clientSocket.send(command.encode())
    data = clientSocket.recv(1024)
    strdata = data.decode()
    print(strdata)
    if strdata == "Invalid entry.":
      return "exit"