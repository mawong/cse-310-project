# This file is all about threading for the server. Each thread contains one client connection.
from threading import Thread
from serverCommands import runcommand
from serverCommandsController import logout_controller

class MyThread(Thread):
    #initializes the thread. Give it a unique id, connection, and address.
    def __init__(self, id, connection, addr):
        Thread.__init__(self)
        self.id = id
        self.connection = connection
        self.addr = addr
        self.running = False
        print("New thread created.")

        self.subbed = []
        self.last_command = ''
        # how many groups we're counting by for ag and sg
        self.N = 0
        # the current group we're at
        self.startN = 0

        # create a list of all subscribed groups
        file = open('users.txt', 'r')
        for line in file:
            tokens = line.split()

            if tokens[1] == self.id:
                # this checks if the user is subscribed to any groups
                if len(tokens) >= 3:
                    subs = tokens[2].split('.')
                    for e in subs:
                        if e != '':
                            self.subbed.append(e)
        self.sortedPosts = []
        self.totalPosts = []
        self.groupID = 0
        file.close()

    #this runs (starts) the thread, like CSE 219.
    def run(self):
        self.running = True
        while self.running:
            message = self.connection.recv(1024)

            command = message.decode()

            if (command == 'logout'):
                logout_controller(self.connection, self.id)
                self._stop()

            else:
                result = runcommand(self.last_command, self.connection, command, self.subbed, self.id, self.N, self.startN, self.sortedPosts, self.totalPosts, self.groupID)
                # if we ran the command ag, we need to store the chosen N
                if 'ag' in command:
                    self.N = result
                    self.startN = 1
                    self.last_command = 'ag'
                # if we ran the command sg, we only care about the starting point
                elif 'sg' in command:
                    self.N = result
                    self.startN = 1
                    self.last_command = 'sg'
                # if we ran the command rg, we only care about the starting point
                elif 'rg' in command:
                    self.N = result
                    self.startN = 1
                    self.last_command = 'rg'
                # the n command updates the range of groups we view, so we must update accordingly
                elif command is 'n':
                    if result != 0:
                        self.startN = result
				# the q command will set the last command to '' if the command before that was ag, sg, or rg
                elif command is 'q':
                    if self.last_command == 'ag':
                        self.last_command = ''
                    elif self.last_command == 'sg':
                        self.last_command = ''
                    elif self.last_command == 'rg':
                        self.last_command = ''
                    elif self.last_command == '1':
                        self.last_command = 'rg'
                elif command.isdigit():
                    if self.last_command == 'rg':
                        self.last_command = '1'
                        self.groupID = result
                        if result == 0:
                            self.last_command = 'rg'
						
    #this method stops the thread (by setting a variable to false
    def _stop(self):
        self.running = False
