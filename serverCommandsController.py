# This file is where the server commands are implemented. All server commands for the server should have one or more methods
# in this file that carries out the action/command.

# import
from time import strftime, gmtime
from _thread import allocate_lock

lock = allocate_lock()

# global variables
default_num_posts = 5
ag_num_posts = -1
sg_num_posts = -1
rg_num_posts = -1
rg_id_lines = -1

usersDict = {}
usersLogged = {}
groupsDict = {}
# links group id to post id (many post id's to group id)
groupsNumPosts = {}
# links post id to group
postsGroup = {}
# links post id to subject
postsSubject = {}
# links post id to author (author is a valid user)
postsAuthor = {}
# links post id to time
postsTime = {}
# links post id to content
posts = {}

read = {}

# create the user dictionary and logged in table
file = open('users.txt', 'r')
for line in file:
    # break the line into user id and user name
    tokens = line.split()
    usersDict[tokens[0]] = tokens[1]
    # when the server starts, no users should be logged in
    usersLogged[tokens[1]] = False
file.close()

# create the group dictionary and its number of posts
file = open('groups.txt', 'r')
for line in file:
    # break the line into group id and group name
    tokens = line.split()
    numLines = len(tokens)
    groupsDict[tokens[0]] = tokens[1]
file.close()

# create the posts dictionary
file = open('posts.txt', 'r')
count = 0
postID = 0
content = ""
for line in file:
    if count == 0:
        tokens = line.split()
        postsGroup[tokens[0]] = tokens[1]
        postID = tokens[0]
        groupsNumPosts.setdefault(tokens[1], [])
        groupsNumPosts[tokens[1]].append(postID)
        count += 1
    elif count == 1:
        postsSubject[postID] = line.strip('\n')
        count += 1
    elif count == 2:
        postsAuthor[postID] = line.strip('\n')
        count += 1
    elif count == 3:
        postsTime[postID] = line.strip('\n')
        count += 1
    elif count == 4:
        if line != '.\n':
            content += line
        else:
            posts[postID] = content
            content = ""
            count = 0
file.close()

# create the read dictionary, each user id holds a list of read posts
file = open('read.txt', 'r')
for line in file:
    # break the line into group id and group name
    tokens = line.split()
    read.setdefault(tokens[0], [])
    for i in range(1, len(tokens)):
        if tokens[i] != '':
            read[tokens[0]].append(tokens[i])
file.close()

# for i in postsGroup:
#     print(i, groupsDict[postsGroup[i]])
# for i in postsSubject:
#     print(i, postsSubject[i])
# for i in postsAuthor:
#     print(i, usersDict[postsAuthor[i]])
# for i in postsTime:
#     print(i, postsTime[i])
# for i in posts:
#     print(i, posts[i])

# This is the controller for when a client wants to logout. It sends 'bye' and then closes the client's connection. Also
# it sets the logged in user to false so the user can log back in.
def logout_controller(socket, name):
    message = 'bye'
    socket.send(message.encode())
    usersLogged[name] = False
    socket.close()
    return 'logout'


# This is the controller for when a client wants to view all groups. It accesses the file containing the information
# about the system and returns the metadata about all groups available (subscribed or not). If there is no argument for
# how many posts the client wants to view at a time, the server gives 5 by default.
def ag_controller(socket, command, subbed):
    # see if there is an argument; if so, the length is 2...else, the length is 1
    message = command.split(' ')
    if len(message) == 1:
        print("Use default.")
        ag_num_posts = default_num_posts
    else:
        print("Use message[1]")
        ag_num_posts = int(message[1])

    # ensure the number of groups we post at a time does not exceed our total number of groups
    if ag_num_posts > len(groupsDict):
        ag_num_posts = len(groupsDict)

    data = ""
    # build up a string of all the groups one line at a time
    for i in range(1, ag_num_posts + 1):
        # indicate if we are subscribed to the group or not
        if str(i) in subbed:
            data += (str(i) + '. (s) ' + groupsDict[str(i)])
        else:
            data += (str(i) + '. ( ) ' + groupsDict[str(i)])
        if i is not ag_num_posts:
            data += "\n"

    # send the groups
    socket.send(data.encode())
    return ag_num_posts


# This is the controller for when a client wants to subscribe to a new group.
def s_controller(socket, command, subbed, name, N, startN):
    N += startN - 1
    message = command.split(' ')
    # all new groups the user is subscribing to
    newSubs = ''
    # base message we return back to the client
    data = 'Subscribed to: '

    # if the length of the message is 1, no groups are specified, we can send an error
    if len(message) == 1:
        error = "No groups specified."
        socket.send(error.encode())
        return
    # for each group specified in the message, check if it's a valid group number
    # if valid, add the group to the user's subbed list
    for group in message:
        if group != 's':
            if int(group) <= N and int(group) >= startN:
                if group not in subbed:
                    subbed.append(group)
                    # update the groups string and return message to reflect the new subscription
                    newSubs += group
                    newSubs += "."
                    data += groupsDict[group]
                    data += " "

    # we need to update our users.txt file. we read the entire file in, make changes to the user's
    # subscriptions, write back out the updated file
    newFile = ''
    if newSubs != '':
        lock.acquire()
        file = open('users.txt', 'r')
        for line in file:
            tokens = line.split()
            # remove the newline characters from each line (we will add them back in later)
            noEnter = line.strip('\n')

            # if we found the line that belongs to the user whose subs we're changing, insert the new groups
            if tokens[1] == name:
                noEnter += newSubs
                newFile += noEnter
            # otherwise just read in the line normally
            else:
                newFile += noEnter

            newFile += "\n"
        file.close()
        lock.release()

        lock.acquire()
        file = open('users.txt', 'w')
        file.write(newFile)
        file.close()
        lock.release()

    if data == 'Subscribed to: ':
        data += "nothing"

    # send the groups
    socket.send(data.encode())

# This is the controller for when a client wants to unsubscribe from a group.
def u_controller(socket, command, subbed, name, N, startN):
    N += startN - 1
    message = command.split(' ')

    # base message we return back to the client
    data = 'Unsubscribed from: '

    subsToRemove = []

    # if the length of the message is 1, no groups are specified, we can send an error
    if len(message) == 1:
        error = "No groups specified."
        socket.send(error.encode())
        return
    # for each group specified in the message, check if it's a valid group number
    # if valid, remove the group from the user's subbed list
    for group in message:
        if group != 'u':
            if int(group) <= N and int(group) >= startN:
                if group in subbed:
                    subbed.remove(group)
                    subsToRemove.append(group)
                    # update the groups string and return message to reflect the removed subscription
                    data += groupsDict[group]
                    data += " "

    # we need to update our users.txt file. we read the entire file in, make changes to the user's
    # subscriptions, write back out the updated file
    newFile = ''
    if len(subsToRemove) > 0:
        lock.acquire()
        file = open('users.txt', 'r')
        for line in file:
            tokens = line.split()
            # remove the newline characters from each line (we will add them back in later)
            noEnter = line.strip('\n')

            # if we found the line that belongs to the user whose subs we're changing, remove the groups
            if tokens[1] == name:
                newLine = tokens[0] + " " + tokens[1] + " "

                # remove the groups
                for group in subbed:
                    if group != '':
                        newLine += group
                        newLine += "."

                newFile += newLine
            # otherwise just read in the line normally
            else:
                newFile += noEnter

            newFile += "\n"
        file.close()
        lock.release()

        lock.acquire()
        file = open('users.txt', 'w')
        file.write(newFile)
        file.close()
        lock.release()

    if data == 'Unsubscribed from: ':
        data += "nothing"

    # send the groups
    socket.send(data.encode())

# This is the controller for when a client wants to view the next N groups.
def n_controller(socket, subbed, N, startN):
    # update our starting index
    startN += N
    # calculate our ending index
    N += startN - 1

    # if our starting index exceeds how many groups we have total, we have finished displaying all groups
    if startN > len(groupsDict):
        error = "exit"
        socket.send(error.encode())
        return 0
    # otherwise just ensure we don't allow values out of our group range
    elif N > len(groupsDict):
        N = len(groupsDict)

    data = ""
    # build up a string of all the groups one line at a time
    for i in range(startN, N + 1):
        # indicate if we are subscribed to the group or not
        if str(i) in subbed:
            data += (str(i) + '. (s) ' + groupsDict[str(i)])
        else:
            data += (str(i) + '. ( ) ' + groupsDict[str(i)])
        if i is not N:
            data += "\n"

    # send the groups
    socket.send(data.encode())
    # make sure we know which index to start from next time
    return startN


# This method checks to see if a user is logged in or not. It updates the users dictionary and the users file if the
# user is new, and sends the response back to the server.
def login_controller(name):
    # check to see if the id is already logged in or not and is unique
    loginResponse = 'login OK'

    # if the user exists in the database
    if (name in usersDict.values()):

        # if they are logged in, do not log in again. otherwise, log them in
        if (usersLogged[name] == True):
            loginResponse = 'Error: Already Logged in'
            print(name, "is already logged in.")
        else:
            loginResponse = 'login OK'
            usersLogged[name] = True
            print(name, "logged in.")

    # if the user does not exist in the database, then log them in
    else:
        loginResponse = 'login OK'
        # the new user is now logged in
        usersLogged[name] = True
        # the users new unique ID is 1 larger than the current largest user ID
        newID = len(usersDict) + 1
        # make sure we add the new user to our dictionary
        usersDict[str(newID)] = name
        print("New user, ", name, ", created and logged in.")
        # write the new user to the users text file, so they exist permanently
        lock.acquire()
        usersFile = open('users.txt', 'a')
        newEntry = str(newID) + " " + name + " \n"
        usersFile.write(newEntry)
        usersFile.close()
        lock.release()

        # write the new user to the read text file
        lock.acquire()
        readFile = open('read.txt', 'a')
        newEntry = str(newID) + " \n"
        readFile.write(newEntry)
        readFile.close()
        lock.release()

        read.setdefault(str(newID), [])

    # return the login response
    return loginResponse


# This is the controller for when a client wants to view subscribed groups. It gets the subscribed groups as an argument
# and sends the metadata about all the subscribed groups and their recent posts. If there is no argument for how many
# groups the client wants to view at a time, the server gives 5 per round by default.
def sg_controller(socket, command, subbed, name):
    global sg_num_posts

    # see if there is an argument; if so, the length is 2...else, the length is 1
    message = command.split(' ')
    if len(message) == 1:
        print("Use default.")
        sg_num_posts = default_num_posts
    else:
        print("Use message[1]")
        sg_num_posts = int(message[1])

    # build up a string of all the subscribed groups one line at a time
    if sg_num_posts <= 0:
        data = 'Error: optional argument is negative.'
    else:
        data = ''

    if sg_num_posts > len(subbed):
        sg_num_posts = default_num_posts

    # get the user id
    user_id = ''
    for id, username in usersDict.items():
        if username == name:
            user_id = id

    # this tells us the place we last were in the range (will be returned for N)
    num_groups = 1
    for i in range(0, sg_num_posts):
        if i < len(subbed):
            group = subbed[i]

            if num_groups > 1:
                data += "\n"
            data += str(num_groups) + '. '
            if group in groupsNumPosts:
                new_posts = []
                for posts in groupsNumPosts[group]:
                    if posts not in read[user_id]:
                        new_posts.append(posts)

                if len(new_posts) > 0:
                    data += str(len(new_posts)) + '\t' + groupsDict[group]
                else:
                    data += '\t' + groupsDict[group]
            else:
                data += '\t' + groupsDict[group]
            num_groups += 1

    # if we get here, we did not fulfill the amount of groups specified before num_groups = sg_num_posts
    # test if data is empty; if so, make data say 'there are no subscribed groups.'
    # if data is not empty, break anyway
    if data == '':
        data = 'You are currently not in any subscribed groups.'

    # send the groups
    socket.send(data.encode())

    # return_startN = startN, sg_num_posts = N
    return sg_num_posts

# This is the controller for when a client wants to unsubscribe from a group (from sg).
def u_sg_controller(socket, command, subbed, name, N, startN):
    N += startN - 1
    message = command.split(' ')

    # base message we return back to the client
    data = 'Unsubscribed from: '

    subsToRemove = []

    # if the length of the message is 1, no groups are specified, we can send an error
    if len(message) == 1:
        error = "No groups specified."
        socket.send(error.encode())
        return
    # for each group specified in the message, check if it's a valid group number
    # if valid, remove the group from the user's subbed list
    sub_length = len(subbed)
    for group in message:
        if group != 'u':
            if int(group) <= N and int(group) >= startN:
                # get the group from the index given by the command and remove it
                removedSub = subbed[int(group) - 1]
                if removedSub in subbed:
                    subsToRemove.append(removedSub)
                    # update the groups string and return message to reflect the removed subscription
                    data += groupsDict[removedSub]
                    data += " "

    # remove the groups now
    for sub in subsToRemove:
        subbed.remove(sub)

    # we need to update our users.txt file. we read the entire file in, make changes to the user's
    # subscriptions, write back out the updated file
    newFile = ''
    if len(subsToRemove) > 0:
        lock.acquire()
        file = open('users.txt', 'r')
        for line in file:
            tokens = line.split()
            # remove the newline characters from each line (we will add them back in later)
            noEnter = line.strip('\n')

            # if we found the line that belongs to the user whose subs we're changing, remove the groups
            if tokens[1] == name:
                newLine = tokens[0] + " " + tokens[1] + " "

                # remove the groups
                for group in subbed:
                    if group != '':
                        newLine += group
                        newLine += "."

                newFile += newLine
            # otherwise just read in the line normally
            else:
                newFile += noEnter

            newFile += "\n"
        file.close()
        lock.release()

        lock.acquire()
        file = open('users.txt', 'w')
        file.write(newFile)
        file.close()
        lock.release()

    if data == 'Unsubscribed from: ':
        data += "nothing"

    # send the groups
    socket.send(data.encode())


# This is the controller for when the client wants to view the next N amount of subscribed groups.
def n_sg_controller(socket, subbed, name, N, startN):
    # update our starting index
    startN += N
    # calculate our ending index
    N += startN - 1

    # if our starting index exceeds how many groups we have total, we have finished displaying all groups
    if startN > len(subbed):
        error = "exit"
        socket.send(error.encode())
        return 0
    # otherwise just ensure we don't allow values out of our group range
    elif N > len(subbed):
        N = len(subbed)

    # get the user id
    user_id = ''
    for id, username in usersDict.items():
        if username == name:
            user_id = id

    data = ""
    num_groups = 1
    # build up a string of all the groups one line at a time
    for i in range(startN, N + 1):
        if (i-1) < len(subbed):
            group = subbed[i-1]

            if num_groups > 1:
                data += "\n"
            data += str(i) + '. '
            if group in groupsNumPosts:
                new_posts = []
                for posts in groupsNumPosts[group]:
                    if posts not in read[user_id]:
                        new_posts.append(posts)

                if len(new_posts) > 0:
                    data += str(len(new_posts)) + '\t' + groupsDict[group]
                else:
                    data += '\t' + groupsDict[group]
            else:
                data += '\t' + groupsDict[group]
            num_groups += 1

    # send the groups
    socket.send(data.encode())
    # make sure we know which index to start from next time
    return startN

# This is the controller for when a client wants to view subscribed groups. It gets the subscribed groups as an argument
# and sends the metadata about all the subscribed groups and their recent posts. If there is no argument for how many
# groups the client wants to view at a time, the server gives 5 per round by default.
def rg_controller(socket, command, subbed, name, sortedPosts, totalPosts):
    global rg_num_posts

    sortedPosts.clear()
    totalPosts.clear()

    # see if there is an argument; if so, the length is 3...else, the length is 2
    message = command.split(' ')
    if len(message) == 2:
        print("Use default.")
        rg_num_posts = default_num_posts
    else:
        print("Use message[2]")
        rg_num_posts = int(message[2])

    # group id as a string
    group = ''
    # get the group id based on the group name
    for gName in groupsDict:
        if groupsDict[gName] == message[1]:
            group = gName
            break

    # make sure we are subscribed to the group requested
    if group not in subbed:
        data = "Error: You are not subscribed to " + message[1]
        socket.send(data.encode())
        return

    if rg_num_posts <= 0:
        data = 'Error: optional argument is negative.'
    else:
        data = ''

    # list of unique post ids that are in the requested group
    gPosts = []

    # fill up our list with post ids
    for post in postsGroup:
        if postsGroup[post] == group:
            gPosts.append(post)

    if rg_num_posts > len(gPosts):
        rg_num_posts = default_num_posts

    # user id as a string
    user = ''
    # get the user id based on the user name
    for uName in usersDict:
        if usersDict[uName] == name:
            user = uName
            break

    count = 0
    # add all posts marked with N to the sorted list first
    for i in range(0, len(gPosts)):
        if gPosts[i] not in read[user]:
            print("N loop: " + gPosts[i])
            if count <= rg_num_posts:
                sortedPosts.append(gPosts[i])
            totalPosts.append(gPosts[i])
            count += 1
    # add the rest of the posts
    for i in range(0, len(gPosts)):
        if gPosts[i] not in totalPosts:
            print("normal loop: " + str(gPosts[i]))
            if count <= rg_num_posts:
                sortedPosts.append(gPosts[i])
            totalPosts.append(gPosts[i])
            count += 1

    # counter for which post we're at
    num_posts = 1
    for i in range(0, rg_num_posts):
        if i < len(sortedPosts):
            post = sortedPosts[i]

            if num_posts > 1:
                data += "\n"
            data += str(num_posts) + '. '
            if post not in read[user]:
                data += "N "
            else:
                data += "  "
            date = postsTime[post].split()
            data += date[1] + " " + date[2] + " " + date[3] + " "
            data += postsSubject[post]

            num_posts += 1

    # test if data is empty; if so, that means this group has no posts
    if data == '':
        data = 'This group has no posts.'

    # send the posts
    socket.send(data.encode())

    # return_startN = startN, rg_num_posts = N
    return rg_num_posts

# This is the controller for when the client wants to view the next N amount of posts
def n_rg_controller(socket, subbed, N, startN, name, sortedPosts, totalPosts):
    # update our starting index
    startN += N
    # calculate our ending index
    N += startN - 1

    sortedPosts.clear()

    # if our starting index exceeds how many posts we have total, we have finished displaying all posts
    if startN > len(totalPosts):
        error = "exit"
        socket.send(error.encode())
        return 0
    # otherwise just ensure we don't allow values out of our posts range
    elif N > len(totalPosts):
        N = len(totalPosts)

    count = 1
    # fill up our list with post ids
    for i in range(0, len(totalPosts)):
        if count >= startN and count <= N:
            sortedPosts.append(totalPosts[i])
        count += 1

    # user id as a string
    user = ''
    # get the user id based on the user name
    for uName in usersDict:
        if usersDict[uName] == name:
            user = uName
            break

    data = ''
    # counter for which post we're at
    index = 0
    num_posts = 1
    for i in range(startN, N + 1):
        post = sortedPosts[index]
        index += 1

        if num_posts > 1:
            data += "\n"
        data += str(i) + '. '
        if post not in read[user]:
            data += "N "
        else:
            data += "  "
        date = postsTime[post].split()
        data += date[1] + " " + date[2] + " " + date[3] + " "
        data += postsSubject[post]

        num_posts += 1

    # test if data is empty; if so, that means this group has no posts
    if data == '':
        data = 'This group has no posts.'

    # send the posts
    socket.send(data.encode())

    # make sure we know which index to start from next time
    return startN

# This is the controller for when a client wants to make a post as read.
def r_controller(socket, command, subbed, name, N, startN, sortedPosts):
    # calculate our ending index
    N = startN + len(sortedPosts) - 1

    message = command.split(' ')

    # base message we return back to the client
    data = 'Post(s) marked as read.'

    # list of unique post ids to mark as read
    postsToMark = []

    # if the length of the message is 1, no posts are specified, we can send an error
    if len(message) == 1:
        error = "No posts specified."
        socket.send(error.encode())
        return

    # post number to mark read
    post = message[1]
    # zero if only one post, non-zero if we have a range of posts
    postEnd = 0

    # check for the character that tells us if we have a range
    if '-' in post:
        tokens = post.split('-')
        post = tokens[0]
        postEnd = tokens[1]
        if int(post) < startN or int(post) > N or int(postEnd) < startN or int(postEnd) > N or int(post) > int(postEnd):
            error = "Invalid post(s) specified."
            socket.send(error.encode())
            return
    else:
        if int(post) < startN or int(post) > N:
            error = "Invalid post(s) specified."
            socket.send(error.encode())
            return

    # if we only have one post to mark as read
    if postEnd == 0:
        postsToMark.append(sortedPosts[int(post) - startN])
    else:
        for i in range(int(post)-startN, int(postEnd)-startN+1):
            postsToMark.append(sortedPosts[i])

    # user id as a string
    user = ''
    # get the user id based on the user name
    for uName in usersDict:
        if usersDict[uName] == name:
            user = uName
            break

    newRead = ''
    for post in postsToMark:
        if post not in read[user]:
            read[user].append(post)
            newRead += str(post) + " "

    # we need to update our read.txt file. we read the entire file in, make changes to the user's
    # read groups, write back out the updated file
    newFile = ''
    if newRead != '':
        lock.acquire()
        file = open('read.txt', 'r')
        for line in file:
            tokens = line.split()
            # remove the newline characters from each line (we will add them back in later)
            noEnter = line.strip('\n')

            # if we found the line that belongs to the user whose read posts we're changing, insert the new read posts
            if tokens[0] == user:
                noEnter += newRead
                newFile += noEnter
            # otherwise just read in the line normally
            else:
                newFile += noEnter

            newFile += "\n"
        file.close()
        lock.release()

        lock.acquire()
        file = open('read.txt', 'w')
        file.write(newFile)
        file.close()
        lock.release()

    # send the groups
    socket.send(data.encode())

# This is the controller for when a user wants to make a new post.
# sent to server: 'p \r\n\r\n[groupname]\r\n\r\n[subject]\r\n\r\n[content]\n.\n'
def p_controller(socket, command, user_name):
    # get the next post id
    post_id = len(posts) + 1

    # and the group name
    data = command.split('\r\n\r\n')
    groupname = data[1]
    group_id = ''
    for id, name in groupsDict.items():
        if name == groupname:
            group_id = id

    # finally, get the subject and content
    subject = data[2]

    content = data[3]

    # and now get the user id
    user_id = ''
    for id, username in usersDict.items():
        if username == user_name:
            user_id = id

    # now, start building for posts.txt
    time = strftime('%a, %b %d %H:%M:%S %Z %Y', gmtime())
    new_post = str(post_id) + ' ' + str(group_id) + '\n'
    new_post += subject + '\n'
    new_post += str(user_id) + '\n'
    new_post += time + '\n'
    new_post += content

    # and adding the stuff to the dicts
    postsGroup[post_id] = group_id
    postsSubject[post_id] = subject
    postsAuthor[post_id] = user_id
    postsTime[post_id] = time
    posts[post_id] = content

    # and update the txt file
    lock.acquire()
    file = open('posts.txt', 'a')
    file.write(new_post)
    file.close()
    lock.release()

    # mark the post as read
    newFile = ''
    lock.acquire()
    file = open('read.txt', 'r')
    for line in file:
        tokens = line.split()
        # remove the newline characters from each line (we will add them back in later)
        noEnter = line.strip('\n')

        # if we found the line that belongs to the user whose read posts we're changing, insert the new read posts
        if tokens[0] == user_id:
            noEnter += str(post_id) + ' '
            newFile += noEnter
        # otherwise just read in the line normally
        else:
            newFile += noEnter

        newFile += "\n"
    file.close()
    lock.release()

    lock.acquire()
    file = open('read.txt', 'w')
    file.write(newFile)
    file.close()
    lock.release()

    read[user_id].append(post_id)

    status = 'p OK'
    socket.send(status.encode())

def id_rg_controller(socket, id, startN, N, totalPosts):
    rg_id_lines = N
    print("lines: " + str(rg_id_lines))
    N += startN - 1

    if int(id) < startN or int(id) > N:
        error = "Invalid entry."
        socket.send(error.encode())
        return 0

    postID = totalPosts[int(id)-1]

    # get the name of the group
    lock.acquire()
    file = open('groups.txt', 'r')
    groupname = ''
    for line in file:
        tokens = line.split()
        # remove the newline characters from each line (we will add them back in later)
        noEnter = line.strip('\n')

        # if we found the line that belongs to the user whose read posts we're changing, insert the new read posts
        if tokens[0] == postsGroup[postID]:
            groupname = tokens[1]
    file.close()
    lock.release()

    # get the name of the author
    lock.acquire()
    file = open('users.txt', 'r')
    authorname = ''
    for line in file:
        tokens = line.split()
        # remove the newline characters from each line (we will add them back in later)
        noEnter = line.strip('\n')

        # if we found the line that belongs to the user whose read posts we're changing, insert the new read posts
        if tokens[0] == postsAuthor[postID]:
            authorname = tokens[1]
    file.close()
    lock.release()

    # print the post information
    finalmessage = ''
    finalmessage += groupname
    finalmessage += '\n'
    finalmessage += postsSubject[postID]
    finalmessage += '\n'
    finalmessage += authorname
    finalmessage += '\n'
    finalmessage += str(postsTime[postID])
    finalmessage += '\n\n'

    # get the lines needed
    contentlines = posts[postID].split('\n')
    i = 0
    while (i < rg_id_lines):
        if (i >= len(contentlines)):
            break
        if (contentlines[i] == '.'):
            break
        finalmessage += contentlines[i]
        finalmessage += '\n'
        i += 1

    # send the post back
    socket.send(finalmessage.encode())

    return id

def n_id_controller(socket, id, startN, N, totalPosts, groupID):
    rg_id_lines = N
    startN += N
    N += startN - 1

    print("startN: " + str(startN))
    print("N: " + str(N))
    print("groupID: " + str(groupID))

    postID = totalPosts[int(groupID)-1]

    # get the name of the group
    lock.acquire()
    file = open('groups.txt', 'r')
    groupname = ''
    for line in file:
        tokens = line.split()
        # remove the newline characters from each line (we will add them back in later)
        noEnter = line.strip('\n')

        # if we found the line that belongs to the user whose read posts we're changing, insert the new read posts
        if tokens[0] == postsGroup[postID]:
            groupname = tokens[1]
    file.close()
    lock.release()

    # get the name of the author
    lock.acquire()
    file = open('users.txt', 'r')
    authorname = ''
    for line in file:
        tokens = line.split()
        # remove the newline characters from each line (we will add them back in later)
        noEnter = line.strip('\n')

        # if we found the line that belongs to the user whose read posts we're changing, insert the new read posts
        if tokens[0] == postsAuthor[postID]:
            authorname = tokens[1]
    file.close()
    lock.release()

    # print the post information
    finalmessage = ''

    # get the lines needed
    contentlines = posts[postID].split('\n')

    # if our starting index exceeds how many posts we have total, we have finished displaying all posts
    if startN > len(contentlines):
        error = "No more content to show."
        socket.send(error.encode())
        return 0
    # otherwise just ensure we don't allow values out of our posts range
    elif N > len(contentlines):
        N = len(contentlines)

    i = startN - 1
    while (i < N):
        if (i >= len(contentlines)):
            break
        if (contentlines[i] == '.'):
            break
        finalmessage += contentlines[i]
        finalmessage += '\n'
        i += 1

    # send the post back
    socket.send(finalmessage.encode())
    return startN

# This lets the user know the server is exiting.
def exit_controller():
    print("exiting")
