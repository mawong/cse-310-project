# This file includes all the commands that the client can do. The client calls runcommand with the command and the name,
# and the command is tested to see which method we go to (which is in the controller file).

# import statements
from clientCommandsController import help_controller, logout_controller, ag_controller, s_controller, sg_controller, n_controller, u_controller, q_controller, rg_controller, r_controller, p_controller, id_controller

# variable to check what was the main command
last_main_command = ''

# This method is the main method in this file. It is called by tcpClient and dictates the flow based on what the command
# is. It takes in the socket fd, command, and the name of the user that is logged in (if the user is logged in; None
# else) as a parameter. It returns true on a successfully processed command, and false otherwise.
def runcommand(clientSocket, command, name):
    global last_main_command
    if last_main_command == '':
        # main commands
        if command == 'help':
            help_controller(name)
        elif command == 'logout':
            logout_controller(name, clientSocket)
            return 1
        elif 'ag' in command:
            last_main_command = 'ag'
            ag_controller(clientSocket, command)
        elif 'sg' in command:
            last_main_command = 'sg'
            sg_controller(clientSocket, command)
        elif 'rg' in command:
            last_main_command = 'rg'
            result = rg_controller(clientSocket, command)
            if result == "exit":
                print("Exited from rg command.")
                last_main_command = ''
                q_controller(clientSocket, 'q', last_main_command)
        else:
            print("Invalid command.")
        return 0
    elif last_main_command == 'ag':
        # sub-commands for ag
        if 's ' in command:
            s_controller(clientSocket, command)
        elif 'u ' in command:
            u_controller(clientSocket, command)
        elif command == 'q':
            print("Exited from ag command.")
            last_main_command = ''
            q_controller(clientSocket, command, last_main_command)
        elif command == 'n':
            result = n_controller(clientSocket, command)
            if result == "exit":
                print("Exited from ag command.")
                last_main_command = ''
                q_controller(clientSocket, 'q', last_main_command)
        elif 'ag' in command:
            print("We're already in ag.")
        elif command == 'help' or command == 'logout':
            print("This command is not accessible while in ag.")
        else:
            print("Invalid command.")
    elif last_main_command == 'sg':
        # sub-commands for sg
        if 'u ' in command:
            u_controller(clientSocket, command)
        elif command == 'n':
            result = n_controller(clientSocket, command)
            if result == "exit":
                print("Exited from sg command.")
                last_main_command = ''
                q_controller(clientSocket, 'q', last_main_command)
        elif command == 'q':
            print("Exited from sg command.")
            last_main_command = ''
            q_controller(clientSocket, command, last_main_command)
        elif 'sg' in command:
            print("We're already in sg.")
        elif command == 'help' or command == 'logout':
            print("This command is not accessible while in sg.")
        else:
            print("Invalid command.")
    elif last_main_command == 'rg':
        # sub-commands for rg
        if command == 'q':
            print("Exited from rg command.")
            last_main_command = ''
            q_controller(clientSocket, command, last_main_command)
        elif 'r ' in command:
            r_controller(clientSocket, command)
        elif command == 'n':
            result = n_controller(clientSocket, command)
            if result == "exit":
                print("Exited from rg command.")
                last_main_command = ''
                q_controller(clientSocket, 'q', last_main_command)
        elif command == 'p':
            p_controller(clientSocket, command)
        elif command.isdigit():
            result = id_controller(clientSocket, command)
            last_main_command = '1'
            if result == "exit":
                last_main_command = 'rg'
        else:
            print("Invalid command.")
    elif last_main_command.isdigit():
        if command == 'q':
            print("Exited from [id] command.")
            q_controller(clientSocket, command, last_main_command)
            last_main_command = 'rg'
        elif (command == 'n'):
            result = n_controller(clientSocket, command)
            if result == "exit":
                print("Exited from [id] command.")
                last_main_command = 'rg'
                q_controller(clientSocket, command)
    else:
        print('Invalid command.')