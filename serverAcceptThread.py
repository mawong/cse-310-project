# This file is for the server; it contains information necessary to accept clients. There should only be one accept
# thread running at all times.
from threading import Thread
from serverThread import MyThread
from serverCommandsController import login_controller
from serverCommandsController import logout_controller
from serverCommandsController import exit_controller
from socket import *

# port
serverPort = 12000

# create, bind, and listen
serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind(('', serverPort))
serverSocket.listen(1)

class AcceptThread(Thread):
    # initializes the thread. Give it the socket that the server is running on.
    def __init__(self):
        Thread.__init__(self)
        self.running = False
        self.threads = []

        #print we are done creating the accept thread
        print("Accept thread created.")
        

    # this runs (starts) the thread, like CSE 219.
    def run(self):
        self.running = True

        # print that the server is ready to receive
        print('The server is ready to receive')

        while self.running:
            # accept the connection and create a thread for the client; run it
            connectionSocket, addr = serverSocket.accept()

            # get the name of the person logging in
            loginRequest = connectionSocket.recv(1024)
            loginRequest = loginRequest.decode()
            loginArgs = loginRequest.split()
            id = loginArgs[1]
            loginResponse = 'login OK'

            if (loginArgs[0] == 'login'):
                loginResponse = login_controller(id)

            # send back the response
            connectionSocket.send(loginResponse.encode())

            # if we logged in, then create a new thread for the connection
            if (loginResponse == 'login OK'):
                thread = MyThread(id, connectionSocket, addr)
                thread.start()
                self.threads.append(thread)

                # if we did not login, then we close the connection
            else:
                connectionSocket.close()

    #this method stops the thread (by setting a variable to false
    def _stop(self):

        #terminate all threads running
        self.running = False
        for i in self.threads:
            i.join()

        print("All client threads terminated.")

        #update the users list
        exit_controller()

        #close the server socket
        serverSocket.close()
