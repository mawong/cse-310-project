# This file is the start of the server. It creates a thread to accept client connections, and then waits for ctrl-c to be
# pressed before exiting the server.

# import the stuff we need for the socket
from serverAcceptThread import AcceptThread
import sys

# this starts the thread that accepts client requests
try:
    thread = AcceptThread()
    thread.start()

    while 1:
        i = 1

# if ctrl-c is hit, then join all threads and exit
except KeyboardInterrupt:
    print('closing server...')
    thread._stop()
    thread.join()
    print('All threads terminated.')
    sys.exit(0)
