# This file includes all the commands that the server can do. The server calls runcommand from the client, and processes
# the request (with help from the controller file).
from serverCommandsController import ag_controller, s_controller, sg_controller, n_controller, u_controller, n_sg_controller, u_sg_controller, rg_controller, r_controller, n_rg_controller, p_controller, id_rg_controller, n_id_controller

# This method parses the commands from the clients. It is called by serverThread and dictates the flow based on what the
# command is. It takes in the client socket, command, the ids of groups the client is subbed to, and the name of the
# user as parameters.
def runcommand(last_main_command, socket, command, subbed, name, N, startN, sortedPosts, totalPosts, groupID):
    if last_main_command == '':
        # handling ag command
        if ('ag' in command):
            last_main_command = 'ag'
            return ag_controller(socket, command, subbed)

        # handling sg command
        elif ('sg' in command):
            last_main_command = 'sg'
            return sg_controller(socket, command, subbed, name)

        # handling rg command
        elif ('rg' in command):
            last_main_command = 'rg'
            return rg_controller(socket, command, subbed, name, sortedPosts, totalPosts)

        else:
            print("invalid command.")

    elif last_main_command == 'ag':
        # handling s command
        if ('s ' in command):
            s_controller(socket, command, subbed, name, N, startN)

        # handling u command
        elif ('u ' in command):
            u_controller(socket, command, subbed, name, N, startN)

        # handling n command
        elif (command == 'n'):
            return n_controller(socket, subbed, N, startN)

        # handling q command
        elif (command == 'q'):
            last_main_command = ''

        else:
            print("invalid command.")

    elif last_main_command == 'sg':
        # handling u command
        if 'u ' in command:
            u_sg_controller(socket, command, subbed, name, N, startN)

        # handling n command
        elif command == 'n':
            return n_sg_controller(socket, subbed, name, N, startN)

        # handling q command
        elif command == 'q':
            last_main_command = ''

        else:
            print("invalid command.")

    elif last_main_command == 'rg':
        # handling q command
        if command == 'q':
            last_main_command = ''
        # handling p command
        elif 'p ' in command:
            p_controller(socket, command, name)
        # handling r command
        elif 'r ' in command:
            r_controller(socket, command, subbed, name, N, startN, sortedPosts)
        elif command == 'n':
            return n_rg_controller(socket, subbed, N, startN, name, sortedPosts, totalPosts)
        elif command.isdigit():
            return id_rg_controller(socket, command, startN, N, totalPosts)
        else:
            print("invalid command.")
    elif last_main_command == '1':
        # handling n command
        if command == 'n':
            return n_id_controller(socket, command, startN, N, totalPosts, groupID)
    else:
        print("invalid command.")
