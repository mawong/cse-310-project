#import the stuff we need for the socket
import sys
from socket import *
from clientCommands import runcommand

# Get the server hostname and port as command line arguments
argv = sys.argv
host = argv[1]
port = int(argv[2])
clientSocket = None

#name is null if you are not logged in
name = None

#do not let any command other than login or logout come through since we need to do those before anything else
while (name == None):

    #get the command
    command = input('>')

    #if we are trying to logout before we login, then just exit the program. there is nothing to close
    if (command == 'logout'):
        print ('logging out and exiting...')
        sys.exit()

    #if we need the help command, print out the help prompt
    elif (command == 'help'):

        #print the help command
        runcommand(None, command, name)

    #otherwise, if login is in the command, then we are trying to log in
    elif ('login ' in command):

        #create a socket and connect to the server
        clientSocket = socket(AF_INET, SOCK_STREAM)
        clientSocket.connect((host,port))

        #send the login request and wait for a response from the server
        command = command.encode()
        clientSocket.send(command)
        message = clientSocket.recv(1024)
        message = message.decode()

        #if the login was successful, then set name to the name we are logging in as
        if (message == 'login OK'):

            #split the command into an array of words split by the ' '
            #commandArgs[0] = 'login'
            #commandArgs[1] = name
            commandArgs = message.split(' ')
            name = commandArgs[1]

        #if we got an error, print it out
        else:
            print(message)

        #if it is an invalid command, let the user know that 'help exists'
    else:
        print('invalid command, type \'help\' to get a list of commands')


running = True

while running:
        
    #receive input from user
    command = input('>')

    #figure out what command was entered and respond appropriately, exit is 0 unless logout was the command
    exit = runcommand(clientSocket, command, name)

    # if our command is logout, we exit
    if exit is 1:
        sys.exit(0)
